#!/usr/bin/env python
import yaml
import optparse
import sys

import dyn_module

if __name__=="__main__":
    parser = optparse.OptionParser("usage: %prog [options]")
    parser.add_option("-c", "--config", dest="config_file",
                      default="DNC.yml", type="string",
                      help="specify the main configuration file")
    parser.add_option("-o", "--output", dest="output_folder",
                      default="output", type="string",
                      help="specify where results will be stored")

    (options, args) = parser.parse_args()
    config_file = options.config_file

     
config_stream = file(config_file, 'r')
config = yaml.load(config_stream)

defaults = config["modules"]["nagios_config"]

hosts = dyn_module.load_module("modules/host_definitions.py").get(config["modules"]["host_definitions"])
datacenters = dyn_module.load_module("modules/datacenter_definitions.py").get(config["modules"]["datacenter_definitions"])

dyn_module.load_module("modules/nagios_config.py").generate(defaults,hosts, datacenters)
