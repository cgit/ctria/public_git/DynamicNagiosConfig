#!/usr/bin/env python
import dyn_module

def get(config):
    result = dict()
    for dc_def_module in config:
        dc_def_mod = dyn_module.load_module("modules/datacenter_definitions/%s.py" % dc_def_module)
        dc_def_mod.get(config[dc_def_module],result)

    return result
