#!/usr/bin/env python
import yaml

def get(config,result):
    dc_config = yaml.load(file(config["file"], 'r'))
    for dc in dc_config:
        if result.has_key(dc):
            result[dc].update(dc_config[dc])
        else:
            result[dc] = dc_config[dc]
            
    return result