#!/usr/bin/env python
from string import Template

def generate(defaults,hosts,datacenters):
    host_template = Template(file("templates/host", 'r').read())
    for host in hosts:
        hostdict = {"hostname": host}
        if hosts[host].has_key("parents"):
            hostdict["parents"] = 'parents   ' + hosts[host]["parents"]
        elif hosts[host].has_key("datacenter") and datacenters.has_key(hosts[host]["datacenter"]):
            hostdict["parents"] = 'parents   ' + datacenters[hosts[host]["datacenter"]]["gateway"]
        else:
            hostdict["parents"] = ''
        
        if hosts[host].has_key("use"):
            hostdict["use"] = hosts[host]["use"]
        else:
            hostdict["use"] = defaults["default_host_use"]
        
        if hosts[host].has_key("alias"):
            hostdict["host_alias"] = hosts[host]["alias"]
        else:
            hostdict["host_alias"] = host
        
        if hosts[host].has_key("address"):
            hostdict["host_address"] = hosts[host]["address"]
        else:
            hostdict["host_address"] = host
        
        print host_template.substitute(hostdict)
        