#!/usr/bin/env python
import dyn_module

def get(config):
    result = dict()
    for host_def_module in config:
        host_def_mod = dyn_module.load_module("modules/host_definitions/%s.py" % host_def_module)
        host_def_mod.get(config[host_def_module],result)

    return result
