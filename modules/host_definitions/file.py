#!/usr/bin/env python
import yaml

def get(config,result):
    hosts_config = yaml.load(file(config["file"], 'r'))
    for host in hosts_config:
        if result.has_key(host):
            result[host].update(hosts_config[host])
        else:
            result[host] = hosts_config[host]
            
    return result