#!/usr/bin/env python
import os

def get(config,result):    
    for host in [ name for name in os.listdir(config["path"]) if os.path.isdir(os.path.join(config["path"], name)) ]:
        if not result.has_key(host):
            result[host] = dict()
        path = os.path.join(config["path"], host, "autoinfo")
        if os.path.exists(os.path.join(path, "datacenter")):
            result[host]["datacenter"] = open(os.path.join(path, "datacenter"),"r").read()
        if os.path.exists(os.path.join(path, "distro")):
            result[host]["distro"] = open(os.path.join(path, "distro"),"r").read()

    return result
